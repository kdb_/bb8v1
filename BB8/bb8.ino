#include <VirtualWire.h>
#include "ServoTimer2.h"

byte buffer[VW_MAX_MESSAGE_LEN];
byte buflen = VW_MAX_MESSAGE_LEN;	

int const radio = 2;
int const udi_p = 3; /* up-down intensity */
int const udd_p = 4; /* up-down direction */
int const s1_p = 5;
int const s2_p = 6;

int const raspi1 = 10;
int const raspi2 = 11;

int const e = 2;

ServoTimer2 servo1;
ServoTimer2 servo2;

void clear() {
	digitalWrite(raspi1, LOW);
	digitalWrite(raspi2, LOW);
	digitalWrite(e, LOW);
}
void setup() {
	pinMode(udi_p, OUTPUT);
	pinMode(udd_p, OUTPUT);
	pinMode(raspi1, OUTPUT);
	pinMode(raspi2, OUTPUT);
	servo1.attach(s1_p);
	servo2.attach(s2_p);

	Serial.begin(9600);
	vw_set_rx_pin(radio);
	vw_setup(1000);
	vw_rx_start();
	Serial.print("begin...");
}

void loop() {
	uint8_t x, y, z;
	if(vw_get_message(buffer, &buflen)){
		clear();
		Serial.print("received ");
		dump(buffer);
		x = buffer[0];
		y = buffer[1];
		z = buffer[2];

		/* Checksum */
		if((buffer[2]&0x80)>>7 == x%2)
			Serial.print("(E)");
		if((buffer[2]&0x40)>>6 == y%2)
			Serial.print("(e)");

		// if((x & 0x7f) > 0) 
		{
			int pos = servoPos(x & 0x7f, x & 0x80);
			servo1.write(pos);
			servo2.write(pos);
			Serial.println(pos);
		}
		// if((y & 0x7f) > 0) 
		{

			if(y & 0x80) 
				digitalWrite(udd_p, HIGH);
			else 
				digitalWrite(udd_p, LOW);

			analogWrite(udi_p, (y & 0x7f) * 2);
		}

		if(z & 1)
			digitalWrite(raspi1, HIGH);
		if(z & 2)
			digitalWrite(raspi2, HIGH);
		
	}
}

void dump(uint8_t *m) {
	char message[50];
	sprintf(message, "%u %u %u \n", m[0], m[1], m[2]);
	Serial.print(message);
}

int servoPos(int val, int dir) {
	int mid = 750;
	int max_var = 500;
	float v = val;
	int dist = (v / 127) * max_var;
	if(dir) dist = 0 - dist;

	// char message[50];
	// sprintf(message, "%d [%d] = (%f / 127) * %d\n", dist, dir, v, max_var);
	// Serial.print(message);

	return mid + dist;
}