/**
 * nunchuk.h
 * library to abstract the use of a nunchuk for arduino
 * Author : Khaïm D. Berkane (namigyj)									2017
 *
 * Credits go to Robert Eisele (infusion) for reversing and documenting the 
 * nunchuk (I also learned how I2C worked thanks to him).
 * 
 * Do what the fuck you want with it.
 **/

/* NOR config
 * |SCL|_|GND| 		SCL-> A5
 * |Vcc   SDA|		SDA-> A4
 */
#ifndef NUNCHUK_H
#define NUNCHUK_H

#include <Arduino.h>
#include <Wire.h>
#include <String.h>

#define NC_BSIZE 	6		/* buffer size */
#define NC_ID 		0x52	/* I2C address of nunchuk */

/* cross-arduino support (Yun & Uno don't use write/send function) */
#if ARDUINO >= 100
#define I2C_READ() Wire.read()
#define I2C_WRITE(x) Wire.write(x)
#else
#define I2C_READ() Wire.receive()
#define I2C_WRITE(x) Wire.send(x)
#endif

class Nunchuk {
public:

	/* start the initializing sequence (with enccryption) */
	void init();

	/* request new data and re-fill the buffer with it */
	bool update();

	/* the original values (raw*()) aren't very practical for motors control
	 * [0 <- 128][128 -> 256] 
	 * we'd rather want a varying value independently of the direction, the 
	 * latter can be specified with a bit (the MSb since the raw value only 
	 * has 128 bit precision anyway...). We obtain in decimal :
	 * [0 -> 127][128 -> 255]
	 * since MSb is the direction : if <128 = down, >128 = up
	 */
	uint8_t norm(uint8_t i);	
	uint8_t x_val();
	uint8_t y_val();

	/* values returned by raw_*() :
	 * the joystick's values vary between [0;255] (in practice: [26;237])
	 * 128 at rest. When going left or down, it decreases towards 0.
	 * And it increases towards 255 when pushing up or right.
	 */
	uint8_t raw_x() const { return _buffer[0]; }
	uint8_t raw_y() const { return _buffer[1]; }
	bool left() const { return _buffer[0] < 112; }
	bool right() const { return _buffer[0] > 144; }
	bool up() const { return _buffer[1] > 144; }
	bool down() const { return _buffer[1] < 112; }

	bool z_pressed() const { return !(_buffer[5] & 0x01); }
	bool c_pressed() const { return !(_buffer[5] & 0x02); }

	// int x_acceleration() const { /* 2 LSbs are in the 6th byte */
	// 	return ((int)(_buffer[2]) << 2) | ((_buffer[5] >> 2 ) & 0x03);
	// }
	// int y_acceleration() const {
	// 	return ((int)(_buffer[3]) << 2) | ((_buffer[5] >> 4 ) & 0x03);
	// }
	// int z_acceleration() const {
	// 	return ((int)(_buffer[4]) << 2) | ((_buffer[5] >> 6 ) & 0x03);
	// }

	/* get one string with all the values (for testing and/or debugging) */
	String print();

private:
	void request_data();
	/* for decryption */
	char decode_byte(const char);

	unsigned char _buffer[NC_BSIZE];
};

#endif