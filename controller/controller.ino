#include <VirtualWire.h>
#include <Wire.h>

#include "nunchuk.h"

#define BUFLEN 3 

int const l = 12;
int const r = 8;
int const u = 7;
int const d = 9;
int const c =  4;
int const z =  5;
int const e =  2;
int const t =  2;

Nunchuk nc;

void clr() {
	digitalWrite(l, LOW);
	digitalWrite(r, LOW);
	digitalWrite(u, LOW);
	digitalWrite(d, LOW);
	digitalWrite(c, LOW);
	digitalWrite(z, LOW);
}
void err() {
	digitalWrite(l, HIGH);
	digitalWrite(r, HIGH);
	digitalWrite(d, HIGH);
	digitalWrite(u, HIGH);
	delay(500);
	clr();
}
void setup() {
	pinMode(e, OUTPUT);
	pinMode(t, OUTPUT);
	pinMode(l, OUTPUT);
	pinMode(r, OUTPUT);
	pinMode(u, OUTPUT);
	pinMode(d, OUTPUT);
	pinMode(c, OUTPUT);
	pinMode(z, OUTPUT);
	/* Bits per sec */
	vw_setup(1000); 
	vw_set_tx_pin(2);
	err();
	nc.init();
	Serial.begin(9600);
}
void loop() {
	/* use nc.update() to read nunchuck */
	uint8_t buffer[3];
	clr();
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	if(nc.update()){
		uint8_t x = nc.x_val();
		uint8_t y = nc.y_val();
		if((x & 0x80) && (x & 0x7f) > 0)
			digitalWrite(r, HIGH);
		else if ((x & 0x7f) > 0)
			digitalWrite(l, HIGH);
		if ((y & 0x80) && (y & 0x7f) > 0)
			digitalWrite(u, HIGH);
		else if ((y & 0x7f) > 0)
			digitalWrite(d, HIGH);
		if(nc.z_pressed()) {
			digitalWrite(z, HIGH);
			buffer[2] += 1;
		}
		if(nc.c_pressed()) {
			digitalWrite(c, HIGH);
			buffer[2] += 2;
		}
		buffer[0] = x;
		buffer[1] = y;

		send(buffer);
	} else {
		//err();
	} 	
	delay(50);
}


void send(uint8_t *message) {
	checksum(message);
	digitalWrite(e,HIGH);
	Serial.print("Transmited: ");
	char print[10];
	sprintf(print, "%u %u %u \n", message[0], message[1], message[2]);
	Serial.println(print);
	Serial.println(nc.print());
	vw_send(message, BUFLEN);
	/* Wait until the whole message is gone */
	vw_wait_tx(); 
	digitalWrite(e,LOW);
}

void checksum(uint8_t *message) {
	message[2] += (message[0]%2) << 7;
	message[2] += (message[1]%2) << 6;
}