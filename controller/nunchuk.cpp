#include <Arduino.h>
#include <Wire.h>
#include <String.h>
#include "nunchuk.h"

void Nunchuk::init() {
	Wire.begin();
	Wire.beginTransmission(NC_ID);
	I2C_WRITE(0x40);
	I2C_WRITE(0x00);
	Wire.endTransmission();
	update();
}

bool Nunchuk::update() {
	delay(1);
	Wire.requestFrom(NC_ID, NC_BSIZE);
	
	int i =0;
	while (Wire.available() && i < NC_BSIZE)
		_buffer[i++] = decode_byte(I2C_READ());

	request_data();
	return i == NC_BSIZE;
}
void Nunchuk::request_data() {
	Wire.beginTransmission(NC_ID);
	I2C_WRITE(0x00);
	Wire.endTransmission();
}

char Nunchuk::decode_byte(const char b) {
	return (b ^ 0x17) + 0x17;
}

String Nunchuk::print() {	
	String s = String(raw_x());
	s += ", ";
	s += raw_y(); s += ", ";
	s += (uint8_t)c_pressed();
	s += (uint8_t)z_pressed();
	return s;
}

/* 128 - i : because we want to increase the value in both directions !
 * if we're up, the MSb is set (cause raw() for up > 128 anyway)
 * finally we discard the last 4bits (to avoid a too sensitive joystick and 
 * heat the motors)
 */
uint8_t Nunchuk::norm(uint8_t i) {
	return (i < 128 ? 128 - i : i) & 0xf0;
}
uint8_t Nunchuk::y_val() {
	return norm(raw_y());
}
uint8_t Nunchuk::x_val() {
	return norm(raw_x());
}
